//for facebook plugin
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.3";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function highlightCurrentPage(query) {
	if (query && query != "o-projektu.cshtml")
		query = "a[href$='" + query + "']";
	else
		query = "a[href$='o-projektu']";
	$(query).addClass("active");
}